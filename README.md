# Boardy

A keyboard focused gitlab board frontend

## Usage

Boardy requires your gitlab token to be present in the `.env` file. The token can
be found in the gitlab settings under `settings/access tokens`

```bash
#.env
GITLAB_TOKEN="<your token here>"
```


You also need a `.boardy.toml` file which tells boardy which repo to look in,
and which issue labels are categories.

```toml
# The URL of the gitlab instance to communicate with. Should not contain the
# protocol, i.e. https://.
url = "gitlab.com"

# The ID of your project. Visible underneath the issue name on the details page
# of the project
project_id = 17406772

# List of labels that correspond to columns on the board
board_labels = ["To Do", "Doing", "QOL"]
```


Once the setup is done, run `bbtx` in the directory where you placed the files. Scrolling is done through `j` and `k`, most other controls are accessed through fuzzy search after pressing `space`

// Copied from TUI examples

use std::io::{Stdin};
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

use mio::{unix::EventedFd, Poll, PollOpt, Ready, Token};

use std::sync::{Arc, Mutex};

use termion::event::Key;
use termion::input::{Keys};

pub enum Event<I> {
    Input(I),
    Tick,
}

/// A small event handler that wrap termion input and tick events. Each event
/// type is handled in its own thread and returned to a common `Receiver`
#[allow(dead_code)] // This seems to be a false positive warning for some reason
pub struct Events {
    rx: mpsc::Receiver<Event<Key>>,
    input_handle: thread::JoinHandle<()>,
    tick_handle: thread::JoinHandle<()>,
}

#[derive(Debug, Clone, Copy)]
pub struct Config {
    pub tick_rate: Duration,
}

impl Default for Config {
    fn default() -> Config {
        Config {
            tick_rate: Duration::from_millis(250),
        }
    }
}


impl Events {
    pub fn new(stdin_keys: Arc<Mutex<Keys<Stdin>>>) -> Events {
        Events::with_config(Config::default(), stdin_keys)
    }

    pub fn with_config(config: Config, stdin_keys: Arc<Mutex<Keys<Stdin>>>)
        -> Events
    {
        let (tx, rx) = mpsc::channel();
        let input_handle = {
            let tx = tx.clone();
            thread::spawn(move || {
                // Set up a mio event listener to block the thread until
                // stdin input is available.
                // See https://www.reddit.com/r/rust/comments/9aid9c/how_can_i_check_using_mio_if_stdin_is_readable/
                let poll = Poll::new().unwrap();
                let mut mio_events = mio::Events::with_capacity(1024);

                let stdin_fd = EventedFd(&0);

                poll.register(
                    &stdin_fd,
                    Token(0),
                    Ready::readable(),
                    PollOpt::level()
                ).unwrap();

                loop {
                    poll.poll(&mut mio_events, None)
                        .expect("Mio event poll failed");

                    if let Some(Ok(key)) = stdin_keys.lock().unwrap().next() {
                        if tx.send(Event::Input(key)).is_err() {
                            return;
                        }
                    }
                }
            })
        };
        let tick_handle = {
            let tx = tx.clone();
            thread::spawn(move || {
                let tx = tx.clone();
                loop {
                    tx.send(Event::Tick).unwrap();
                    thread::sleep(config.tick_rate);
                }
            })
        };
        Events {
            rx,
            input_handle,
            tick_handle,
        }
    }

    pub fn next(&self) -> Result<Event<Key>, mpsc::RecvError> {
        self.rx.recv()
    }
}

use gitlab::{Gitlab, Issue, ProjectId, IssueInternalId, Label};

use std::sync::mpsc::{Sender, Receiver};
use std::thread;
use std::collections::HashSet;


type Msg1<A, Msg> = Box<dyn Fn(Result<A, String>) -> Msg + Send + Sync>;

#[allow(clippy::large_enum_variant)]
pub enum Command<Msg> {
    OpenIssue(ProjectId, IssueInternalId, Msg1<Issue, Msg>),
    CloseIssue(ProjectId, IssueInternalId, Msg1<Issue, Msg>),
    FetchIssues(ProjectId, Msg1<Vec<Issue>, Msg>),
    FetchLabels(ProjectId, Msg1<Vec<Label>, Msg>),
    SetLabels(ProjectId, IssueInternalId, HashSet<String>, Msg1<Issue, Msg>),
    SetIssueDescription(ProjectId, IssueInternalId, String, Msg1<Issue, Msg>),
    CreateIssue(ProjectId, Issue, Msg1<Issue, Msg>),
}


pub fn run_api_thread<Msg: 'static + Send + Sync>(
    gitlab: Gitlab,
    source: Receiver<Command<Msg>>,
    sink: Sender<Msg>
) {
    thread::spawn(move || {
        while let Ok(cmd) = source.recv() {
            let msg = match cmd {
                // TODO: maybe replace all these matches with a macro, blyat
                Command::OpenIssue(project, issue, msg) => {
                    msg(
                        gitlab.re_open_issue(project, issue)
                            .map_err(|e| format!("{:?}", e))
                    )
                }
                Command::CloseIssue(project, issue, msg) => {
                    msg(
                        gitlab.close_issue(project, issue)
                            .map_err(|e| format!("{:?}", e))
                    )
                }
                Command::FetchIssues(project, msg) => {
                    msg(
                        gitlab.issues(project, &[("", "")])
                            .map_err(|e| format!("{:?}", e))
                    )
                }
                Command::FetchLabels(project, msg) => {
                    msg (
                        gitlab.labels(project)
                            .map_err(|e| format!("{:?}", e))
                    )
                }
                Command::SetLabels(project, iid, labels, msg) => {
                    msg (
                        gitlab.set_issue_labels(project, iid, labels.iter())
                            .map_err(|e| format!("{:?}", e))
                    )
                }
                Command::SetIssueDescription(project, iid, description, msg) => {
                    msg (
                        gitlab.set_issue_description(project, iid, description)
                            .map_err(|e| format!("{:?}", e))
                    )
                }
                Command::CreateIssue(project, issue, msg) => {
                    msg (
                        gitlab.create_issue(project, issue)
                            .map_err(|e| format!("{:?}", e))
                    )
                }
            };
            sink.send(msg).unwrap();
        }
    });
}

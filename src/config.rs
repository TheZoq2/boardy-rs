use std::fs::File;
use std::io::Read;

use serde_derive::Deserialize;

use toml;

use gitlab::ProjectId;

pub const DEFAULT_CONFIG: &str = include_str!("default_boardy.toml");

#[derive(PartialEq)]
#[derive(Deserialize)]
pub struct Config {
    pub gitlab_token: String,
    pub url: String,
    pub project_id: ProjectId,
    pub board_labels: Vec<String>,
}

impl Config {
    pub fn from_file(path: &str) -> Self {
        match Self::try_read_file(path) {
            Err(e) => {
                println!("======= Example config =======");
                println!("{}", DEFAULT_CONFIG);
                panic!("\n\n\nFailed to read `.boardy.toml`, an example config is printed above\nError: {}", e)
            }
            Ok(content) => {
                toml::from_str(&content)
                    .expect("Failed to parse config")
            }
        }
    }

    fn try_read_file(path: &str) -> Result<String, String> {
        let mut file = File::open(path)
            .map_err(|e| format!("Failed to open file {:?}", e))?;
        let mut content = String::new();
        file.read_to_string(&mut content)
            .map_err(|e| format!("Failed to read file {:?}", e))?;
        Ok(content)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    use toml::value;

    #[test]
    fn default_config_is_correct() {
        let parsed = toml::from_str::<Config>(DEFAULT_CONFIG);
        assert!(parsed.is_ok())
    }
}



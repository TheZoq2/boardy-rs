use std::io;
use std::sync::mpsc::channel;
use std::collections::HashSet;
use std::sync::{Arc, Mutex};
use std::path::PathBuf;

use termion::event::Key;
use termion::raw::IntoRawMode;
use tui::Terminal;
use tui::backend::TermionBackend;
use termion::input::{TermRead};

use birch::line_input::handle_key_input;

use ::gitlab::{Gitlab, Issue};

mod event;
mod msg;
mod model;
mod view;
mod commands;
mod gitlab;
mod text_editor;
mod config;

use model::Model;
use msg::{Msg, Cmd, ScrollDirection};

use event::{Event, Events};


#[macro_use] extern crate log;
use simplelog::{CombinedLogger, WriteLogger};

macro_rules! loopback {
    ($($msg:expr),*) => (vec![$(Cmd::Loopback($msg)),*])
}
macro_rules! gitlab {
    ($cmd:ident($($arg:expr),*) => $msg:ident) => {
        vec!(Cmd::Gitlab(gitlab::Command::$cmd($($arg),*, Box::new(Msg::$msg))))
    }
}

/**
  Performs the specified update function if the specified issue exists,
  otherwise, reports an error to the model
*/
fn try_update_with_issue(
    model: Model,
    issue_title: &str,
    on_issue: impl FnOnce(Model, &Issue) -> (Model, Vec<Cmd>)
) -> (Model, Vec<Cmd>) {
    let issue = model.issues.iter().find(|i| i.title == issue_title).cloned();
    match issue {
        Some(issue) => {
            let (mut model, cmd) = on_issue(model, &issue);
            model.issues_waiting_for_update.push(issue.iid);
            (model, cmd)
        }
        None => {
            (
                model.with_added_error(format!(
                    "Failed to add labels to issue {}. No such issue",
                    issue_title
                )),
                vec!()
            )
        }
    }
}

fn try_update_issue_labels(
    model: Model,
    issue_name: String,
    updater: impl FnOnce(&Vec<String>) -> HashSet<String>
)
    -> (Model, Vec<Cmd>)
{
    try_update_with_issue(
        model,
        &issue_name,
        |model, Issue {iid, labels: old_labels, ..}| {
            let new_labels = updater(old_labels);
            let cmd = gitlab!(
                SetLabels(model.project_id, *iid, new_labels) => IssueUpdate
            );
            (model, cmd)
        }
    )
}

fn try_edit_description(model: Model, issue_title: &str) -> (Model, Vec<Cmd>) {
    try_update_with_issue(model, issue_title, |model, issue| {
        let current_description = issue.description.clone()
            .unwrap_or_else(|| "".into());
        let edit_result = text_editor::edit_text(
            model.stdin_keys.clone(),
            "md",
            &current_description,
            |edited_text| Msg::SetDescription(issue.iid, edited_text)
        );

        match edit_result {
            Ok(msg) => (model, vec!(Cmd::Loopback(msg))),
            Err(e) => {
                (model.with_added_error(format!("{:?}", e)), vec!())
            }
        }
    })
}



fn update(msg: Msg, mut model: Model) -> (Model, Vec<Cmd>) {
    match msg {
        Msg::OpenIssue(title) => {
            try_update_with_issue(model, &title, |model, issue| {
                let cmd = gitlab!(OpenIssue(model.project_id, issue.iid) => IssueUpdate);
                (model, cmd)
            })
        }
        Msg::CloseIssue(title) => {
            try_update_with_issue(model, &title, |model, issue| {
                let cmd = gitlab!(CloseIssue(model.project_id, issue.iid) => IssueUpdate);
                (model, cmd)
            })
        }
        Msg::CreateIssue(title, labels) => {
            let project = model.project_id;
            let user = model.current_user.clone().into();
            let issue = Issue::new(project, title, user).with_labels(labels);
            let cmd = gitlab!(CreateIssue(model.project_id, issue) => IssueCreated);
            (model, cmd)
        }
        Msg::SetDescription(issue_id, description) => {
            let cmd = gitlab!(SetIssueDescription(
                model.project_id,
                issue_id,
                description
            ) => IssueUpdate);
            (model, cmd)
        }
        Msg::StartEditDescription(issue_title) => {
            try_edit_description(model, &issue_title)
        }
        Msg::LabelsFetched(labels) => {
            (model.try_update(Model::set_labels, labels), vec!())
        }
        Msg::IssuesFetched(issues) => {
            (model.try_update(Model::set_issues, issues), vec!())
        }
        Msg::IssueUpdate(issue) => {
            (model.try_update(Model::update_issue, issue), vec!())
        }
        Msg::IssueCreated(issue) => {
            match issue {
                Ok(issue) => {
                    try_edit_description(model.update_issue(issue.clone()), &issue.title)
                }
                Err(e) => (model.with_added_error(e), vec!())
            }
        }
        Msg::RefreshLabels => {
            let cmd = gitlab!(FetchLabels(model.project_id) => LabelsFetched);
            (model, cmd)
        }
        Msg::RefreshIssues => {
            let cmd = gitlab!(FetchIssues(model.project_id) => IssuesFetched);
            (model, cmd)
        }
        Msg::AddLabel(issue_name, labels) => {
            let updater = |old_labels: &Vec<String>| {
                old_labels.iter().cloned()
                    .chain(labels.iter().cloned())
                    .collect::<HashSet<_>>()
            };
            try_update_issue_labels( model, issue_name, updater)
        }
        Msg::RemoveLabels(issue_name, labels) => {
            let updater = |old_labels: &Vec<String>| {
                old_labels.iter().cloned()
                    .filter(|l| !labels.contains(l))
                    .collect::<HashSet<_>>()
            };
            try_update_issue_labels(model, issue_name, updater)
        }
        Msg::ModifyIssueList(issue_name, label) => {
            let new_labels = if label == "" {vec!()} else {vec![label]};
            let board_labels = model.board_labels.clone();
            let updater = move |old_labels: &Vec<String>| {
                old_labels.iter().cloned()
                    .filter(|l| !board_labels.contains(l))
                    .chain(new_labels.into_iter())
                    .collect::<HashSet<_>>()
            };
            try_update_issue_labels(model, issue_name, updater)
        }
        Msg::OpenCommandLine => {
            (model.set_cli_input(Some("".into())), vec!())
        }
        Msg::CloseCommandLine => {
            (model.set_cli_input(None), vec!())
        }
        Msg::AcceptCommandLine => {
            model.accept_input()
        }
        Msg::SetCommandLine(content) => {
            (model.set_cli_input(Some(content)), vec![])
        }
        Msg::Scroll(dir) =>{
            let scroll_amount = match dir {
                ScrollDirection::Up => -1,
                ScrollDirection::Down => 1,
            };
            let new_level = model.scroll_level as i32 + scroll_amount;
            model.scroll_level = if new_level < 0 {0} else {new_level} as u16;
            (model, vec!())
        }
        Msg::Exit => {
            unreachable!("Should be caught in event loop")
        },
        Msg::KeyInput(key) => {
            // Check if this is a global command
            if key == Key::Ctrl('c') || key == Key::Ctrl('d') {
                (model, loopback!(Msg::Exit))
            }
            else if let Some(cli) = &model.cli {
                let result = handle_key_input(
                        cli.current_input.clone(),
                        key,
                        Msg::CloseCommandLine,
                        Msg::AcceptCommandLine,
                        |s| Msg::SetCommandLine(s.to_string()),
                    )
                    .map(|msg| loopback!(msg))
                    .unwrap_or_else(|| vec![]);
                (
                    model,
                    result
                )
            }
            else {
                match key {
                    Key::Char(' ') => (model, vec![Cmd::Loopback(Msg::OpenCommandLine)]),
                    Key::Char('j') =>
                        (model, vec![Cmd::Loopback(Msg::Scroll(ScrollDirection::Down))]),
                    Key::Char('k') =>
                        (model, vec![Cmd::Loopback(Msg::Scroll(ScrollDirection::Up))]),
                    Key::Char('q') =>
                        (model, vec![Cmd::Loopback(Msg::Exit)]),
                    Key::Char('r') =>
                        (model, loopback![Msg::RefreshIssues, Msg::RefreshLabels]),
                    _ => (model, vec!())
                }
            }
        }
    }
}



fn main() -> Result<(), io::Error> {
    let log_file = PathBuf::from(
        format!("/tmp/boardy/{}.log", std::process::id())
    );
    match std::fs::create_dir_all(log_file.parent().unwrap()) {
        Ok(_) => {
            println!(
                "Logging to {:?}, run `cat boardy.log` to start if it is a fifo",
                log_file
            );
            let log_result = CombinedLogger::init(
                vec![WriteLogger::new(
                    simplelog::LevelFilter::Info,
                    simplelog::Config::default(),
                    std::fs::File::create(log_file).unwrap()
                )]
            );
            if let Err(e) = log_result {
                println!("Logger failed to initialise. Logging disabled");
                println!("Error: {:?}", e);
            }
        }
        Err(e) => {
            println!("Failed to create dir for log storage. Logging disabled");
            println!("Error: {:?}", e);
        }
    }


    info!("Starting");

    println!("Trying to read config");
    let config = config::Config::from_file(".boardy.toml");
    let gitlab_token = config.gitlab_token;

    // Set up gitlab
    let (gl_command_tx, gl_command_rx) = channel();
    let (gl_msg_tx, gl_msg_rx) = channel::<Msg>();
    let gitlab = Gitlab::new(config.url, gitlab_token)
        .expect("Failed to initialise gitlab struct");

    println!("Getting user");
    let user = gitlab.current_user().expect("Failed to fetch current user");

    crate::gitlab::run_api_thread(gitlab, gl_command_rx, gl_msg_tx);

    // Set up stdin
    let stdin_keys = Arc::new(Mutex::new(io::stdin().keys()));

    // Set up the model
    let mut model = Model::new(
        config.project_id,
        config.board_labels,
        user,
        stdin_keys.clone()
    );

    // Set up termion
    let stdout = io::stdout().into_raw_mode()?;
    let backend = TermionBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    terminal.clear()?;

    let events = Events::new(stdin_keys);

    // Messages received in this iteration
    let mut msgs = vec!(Msg::RefreshIssues, Msg::RefreshLabels);
    'main: loop {
        // Check input
        #[allow(clippy::single_match)]
        match events.next().unwrap() {
            Event::Input(key) => {
                msgs.push(Msg::KeyInput(key));
            }
            _ => {}
        }

        // Check for messages from other threads
        while let Some(msg) = gl_msg_rx.try_iter().next() {
            msgs.push(msg)
        }

        // Update the model with all new messages
        while let Some(msg) = msgs.pop() {
            // Exit messages have to be handled here, otherwise the terminal
            // does not get cleaned up
            if let Msg::Exit = msg {
                break 'main Ok(());
            }
            let (new_model, new_cmds) = update(msg, model);
            model = new_model;
            // Run commands
            for cmd in new_cmds {
                match cmd {
                    Cmd::Loopback(msg) => msgs.push(msg),
                    Cmd::Gitlab(command) => gl_command_tx.send(command)
                        .expect("Failed to send command")
                }
            }
        }

        view::draw(&mut terminal, &model)?;
    }
}

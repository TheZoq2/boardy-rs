use std::sync::{Arc, Mutex};
use std::process::{Command, Stdio};
use std::io::Stdin;

use std::io::{Write, Read};
use std::path::PathBuf;
use std::fs::create_dir_all;
use std::hash::{Hash, Hasher};
use std::collections::hash_map::DefaultHasher;

use termion::input::Keys;

pub fn edit_text<Msg>(
    stdin_keys: Arc<Mutex<Keys<Stdin>>>,
    filetype: &str,
    content: &str,
    on_close: impl Fn(String) -> Msg
) -> Result<Msg, std::io::Error> {
    let _guard = stdin_keys.lock().unwrap();

    let mut hasher = DefaultHasher::new();
    content.hash(&mut hasher);
    let path = PathBuf::from(format!(
        "/tmp/boardy/{}.{}",
        hasher.finish(),
        filetype
    ));

    create_dir_all(&path.parent().unwrap())?;

    {
        let mut file = std::fs::File::create(&path)?;
        file.write_all(content.as_bytes())?;
    }

    Command::new("nvim")
        .arg(&path)
        .stdin(Stdio::inherit())
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .output().unwrap();

    let mut result = String::new();
    let mut file = std::fs::File::open(&path)?;
    file.read_to_string(&mut result)?;
    std::fs::remove_file(&path)?;

    Ok(on_close(result))
}

use gitlab::{Issue, Label, IssueInternalId};

pub enum ScrollDirection {
    Up,
    Down
}

#[allow(clippy::large_enum_variant)]
pub enum Msg {
    // Command line related messages
    OpenCommandLine,
    CloseCommandLine,
    AcceptCommandLine,
    SetCommandLine(String),
    KeyInput(termion::event::Key),

    Scroll(ScrollDirection),

    CloseIssue(String),
    OpenIssue(String),
    CreateIssue(String, Vec<String>),
    StartEditDescription(String),
    SetDescription(IssueInternalId, String),
    AddLabel(String, Vec<String>),
    /// Moves an issue from the current list to the specified list
    /// If the target issue is `""`, then no label is added
    ModifyIssueList(String, String),
    RemoveLabels(String, Vec<String>),

    RefreshIssues,
    RefreshLabels,
    IssuesFetched(Result<Vec<Issue>, String>),
    LabelsFetched(Result<Vec<Label>, String>),

    IssueUpdate(Result<Issue, String>),
    IssueCreated(Result<Issue, String>),

    Exit
}

pub enum Cmd {
    Loopback(Msg),
    Gitlab(crate::gitlab::Command<Msg>)
}

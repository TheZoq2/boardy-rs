use std::io::Result;

use itertools::Itertools;

use tui::backend::Backend;
use tui::terminal::Frame;
use tui::Terminal;
use tui::widgets::{Widget, Block, Borders, Paragraph, Text, List};
use tui::layout::{Layout, Constraint, Direction, Rect};
use tui::style::{Style, Color, Modifier};

use birch::line_input::draw_command_line;

use gitlab::Issue;

use crate::model::Model;


fn draw_issues(
    f: &mut Frame<impl Backend>,
    issues: &[Issue],
    list_area: Rect,
    model: &Model
) {
    let amount_to_hide_from_scroll = model.scroll_level;

    let chunks = Layout::default()
        .direction(Direction::Vertical)
        .constraints(
            // Inclusive range to avoid having one issue
            // fill the whole area
            (0..=issues.len())
                .map(|_| Constraint::Length(4))
                .collect::<Vec<_>>()
            )
        .split(list_area);

    let issues_to_draw = issues.iter()
            .skip(amount_to_hide_from_scroll as usize)
            .zip(chunks.into_iter());

    for (issue, chunk) in issues_to_draw {
        // Determine if this issue is being updated right now. If so,
        // draw all the colours gray
        let is_being_updated = model.issues_waiting_for_update.contains(&issue.iid);
        let maybe_fg = |c| {
            Style::default()
                .fg(if !is_being_updated {c} else {Color::DarkGray})
        };

        let mut block = Block::default()
            .border_style(Style::default().fg(Color::Gray))
            .borders(Borders::TOP);
        block.render(f, chunk);

        let area = block.inner(chunk);

        let id_text = format!("#{} ", issue.iid);
        let description_icon_text = issue.description.as_ref().map(|d| {
            if d == "" {
                ""
            }
            else {
                "🗪"
            }
        }).unwrap_or_else(|| "");


        // We reserve one line for drawing labels. Casting required because sometimes
        // we end up with areas outside the screen
        let max_length = (area.width as i32 * (area.height as i32 - 1)) as usize;
        let full_title_length = issue.title.len() + id_text.len() + description_icon_text.len();

        let (title_length, extra_str) = if full_title_length > max_length as usize {
            // If we have a smaller area than we expected so we can't even fit issue
            // id and description text, we'll just use the original area. This likely
            // means we are outside the drawable area
            if id_text.len() + description_icon_text.len() + 1 >= max_length as usize {
                (full_title_length, "")
            }
            else {
                // -1 to make room for ellipsis
                (max_length as usize - id_text.len() - description_icon_text.len() - 1, "…")
            }
        }
        else {
            (full_title_length, "")
        };

        let rendered_title = issue.title.chars().take(title_length).collect::<String>() + extra_str;
        let title_text = Text::styled(
            rendered_title,
            maybe_fg(Color::Reset).modifier(Modifier::BOLD)
        );

        let description_icon = Text::Styled(
            description_icon_text.into(),
            maybe_fg(Color::LightBlue).modifier(Modifier::BOLD)
        );

        let label_text_fragments = issue.labels.iter()
            .filter(|l| !model.board_labels.contains(l))
            .map(|x| {
                let color = model.labels.get(x).map(|label| label.color)
                    .unwrap_or(Color::Gray);
                Text::styled(x, maybe_fg(color))
            });

        let issue_id = Text::styled(id_text, maybe_fg(Color::Gray));

        let padding = Text::styled(" | ", maybe_fg(Color::Gray));
        // This iterator -> vec conversion seems to be required because
        // Paragraph wants an iterator of references, not owned text
        let all_fragments = vec!(issue_id, title_text, description_icon).into_iter()
            .chain(vec!(Text::styled("\n", Style::default())).into_iter())
            .chain(label_text_fragments.intersperse(padding))
            .collect::<Vec<_>>();

        Paragraph::new(all_fragments.iter())
            .wrap(true)
            .render(f, area);

    }
}


fn draw_list(
    f: &mut Frame<impl Backend>,
    (title, issues): &(String, Vec<Issue>),
    area: Rect,
    model: &Model,
) {
    let border_color = if title == "open" {
        Color::Gray
    }
    else {
        model.labels.get(&title.to_string()).map(|label| label.color)
            .unwrap_or(Color::Gray)
    };

    let mut block = Block::default()
         .title(title)
         .border_style(Style::default().fg(border_color))
         .borders(Borders::LEFT);

    let issue_area = block.inner(area);

    draw_issues(f, issues, issue_area, model);

    block.render(f, area);
}



fn draw_lists(f: &mut Frame<impl Backend>, area: Rect, model: &Model) {
    let boards = model.boards();
    let constraints =
        (0..=boards.len()).map(|_| Constraint::Length(40));
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .margin(0)
        .constraints(constraints.collect::<Vec<_>>())
        .split(area);

    for (list, chunk) in boards.iter().zip(chunks) {
        draw_list(
            f,
            list,
            chunk,
            model,
        );
    }

}


// Draws the main content. Returns the location of the cursor if it should
// be rendered
pub fn draw_main(f: &mut Frame<impl Backend>, area: Rect, model: &Model) -> Option<(u16, u16)>{
    match &model.cli {
        Some(cli) => {
            let chunks = Layout::default()
                .direction(Direction::Vertical)
                .constraints([
                    Constraint::Min(1),
                    Constraint::Length(15)
                ].as_ref())
                .split(area);

            draw_lists(f, chunks[0], model);

            let (cursor_x, cursor_y) = draw_command_line(f, chunks[1], cli);
            Some((cursor_x + area.left(), cursor_y + area.top()))
        }
        None => {
            draw_lists(f, area, model);
            None
        }
    }
}

// Draw the whole TUI.
pub fn draw(terminal: &mut Terminal<impl Backend>, model: &Model) -> Result<()>{
    let mut cursor_position = None;
    terminal.draw(|mut f| {
        let errors_to_draw = &model.errors.iter()
            .rev()
            .take(5)
            .collect::<Vec<_>>();
        let chunks = Layout::default()
            .direction(Direction::Vertical)
            .constraints([
                Constraint::Length(errors_to_draw.len() as u16),
                Constraint::Min(1),
            ].as_ref())
            .split(f.size());


        List::new(errors_to_draw.iter().
                map(|ref e| Text::raw((*e).to_string()))
            )
            .render(&mut f, chunks[0]);
        cursor_position = draw_main(&mut f, chunks[1], model);
    })?;

    match cursor_position {
        Some(pos) => {
            terminal.show_cursor()?;
            terminal.set_cursor(pos.0, pos.1)
        },
        None => terminal.hide_cursor()
    }
}
